FROM odoo:16

USER root

COPY ./config /etc/odoo

COPY ./base_addons /mnt/extra-addons/base_addons

COPY ./custom_addons /mnt/extra-addons/custom_addons

ENV HOST 188.166.104.18

ENV USER odoo

ENV PASSWORD odoo

EXPOSE 8069